
// STM32_USART_IAP_20170423Dlg.h : 头文件
//

#pragma once
#include "FileDesAes_Dlg.h"

// CSTM32_USART_IAP_20170423Dlg 对话框
class CSTM32_USART_IAP_20170423Dlg : public CDialogEx
{
// 构造
public:
	CSTM32_USART_IAP_20170423Dlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_STM32_USART_IAP_20170423_DIALOG };
	FileDesAes_Dlg * FAesDes_dlg;    //文件加密 窗口
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持
	//设置串口打开
	void Set_OpenPortCom(void);
	//设置串口关闭
	void Set_ClosePortCom(void);
	//导入升级文件数据
	bool SeleupdataFile(LPCTSTR filepath);
// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCbnDropdownCombo1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedCheck1();

//	afx_msg void OnClose();
};
