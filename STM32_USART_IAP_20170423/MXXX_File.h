#pragma once
class CMXXX_File
{
public:
	CMXXX_File();
	~CMXXX_File();


public:
	typedef struct
	{
		DWORD32  UserSize;                     //自定义附加数据长度 4字节
		TBYTE  *User_Data;     //自定义附加数据
		DWORD32  size;                         //4字节文件大小
		TBYTE * FileData;                      //文件数据
	}Mxxx_file_Typ;

	//打开文件   装载文件数据
	bool Install_File_Path(CString filepath);

	//执行文件加密
	//Userstring 用户自定义附加数据
	//DesFlat  真 为DES  否则为AES
	bool File_Encrypt_exe(CString Userstring,TBYTE *Ivdata,TBYTE *Keydata,WORD KeySize,bool DesFlat);

	//执行文件解密
	bool CMXXX_File::File_Decode_exe(CString *Userstring, TBYTE *Ivdata, TBYTE * Keydata, WORD KeySize, bool DesFlat);

	//保存加密后的数据  内部创建文件 写文件
	bool Save_EncryptFile(CString filepath);

	//保存解密后的数据  内部创建文件 写文件
	bool Save_DecodeFile(CString filepath);


	//DES加密 CBC  KeySize 只能为 8 16  24   
	bool Des_Encrypt_CBCString(TBYTE* IV, TBYTE *mes, TBYTE *EnData, TBYTE* Key, WORD KeySize, DWORD64 mesSize);
	//AES加密 CBC  KeySize 只能为 16 24  32 
	bool Aes_Encrypt_CBCString(TBYTE* IV, TBYTE *mes, TBYTE *EnData, TBYTE* Key, WORD KeySize, DWORD64 mesSize);

	//DES解密 CBC  KeySize 只能为 8 16  24   decode
	bool Des_Decode_CBCString(TBYTE* IV, TBYTE *mes, TBYTE *EnData, TBYTE* Key, WORD KeySize, DWORD64 mesSize);
	//AES解密 CBC  KeySize 只能为 16 24  32 
	bool Aes_Decode_CBCString(TBYTE* IV, TBYTE *mes, TBYTE *EnData, TBYTE* Key, WORD KeySize, DWORD64 mesSize);
private:
	//操作后的文件数据大小
	DWORD32  m_EnDeFileSize;
	//操作后的文件数据
	PTBYTE m_EnDeFileData;

	//文件数据大小
	DWORD32  m_FileSize;
	//文件数据
	PTBYTE m_FileData;
};





