// FileDesAes_Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "STM32_USART_IAP_20170423.h"
#include "FileDesAes_Dlg.h"
#include "afxdialogex.h"


// FileDesAes_Dlg 对话框

IMPLEMENT_DYNAMIC(FileDesAes_Dlg, CDialogEx)

FileDesAes_Dlg::FileDesAes_Dlg(CWnd* pParent /*=NULL*/)
: CDialogEx(FileDesAes_Dlg::IDD, pParent)
, m_Userstr(_T(""))
, m_IVStr(_T(""))
, m_KeyStr(_T(""))
, m_Openfilepath(_T(""))
, m_EnFilePath(_T(""))
, m_Defilepath(_T(""))
{
	m_help_dlg = NULL;
}

FileDesAes_Dlg::~FileDesAes_Dlg()
{
}

void FileDesAes_Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT9, m_Userstr);
	DDX_Text(pDX, IDC_EDIT_IV, m_IVStr);
	DDX_Text(pDX, IDC_EDIT_KEY, m_KeyStr);
	DDX_Text(pDX, IDC_EDIT2, m_Openfilepath);
	DDX_Text(pDX, IDC_EDIT5, m_EnFilePath);
	DDX_Text(pDX, IDC_EDIT6, m_Defilepath);
}


BEGIN_MESSAGE_MAP(FileDesAes_Dlg, CDialogEx)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON5, &FileDesAes_Dlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON2, &FileDesAes_Dlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_RADIODes, &FileDesAes_Dlg::OnBnClickedRadiodes)
	ON_BN_CLICKED(IDC_RADIOAes, &FileDesAes_Dlg::OnBnClickedRadioaes)
	ON_BN_CLICKED(IDC_BUTTON3, &FileDesAes_Dlg::OnBnClickedButton3)
	ON_EN_CHANGE(IDC_EDIT9, &FileDesAes_Dlg::OnEnChangeEdit9)
	ON_BN_CLICKED(IDC_BUTTON4, &FileDesAes_Dlg::OnBnClickedButton4)
END_MESSAGE_MAP()


// FileDesAes_Dlg 消息处理程序
void FileDesAes_Dlg::OnClose()
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
	// 获得父窗口的句柄  
	HWND hWnd = this->GetParent()->GetSafeHwnd();

	// 向父窗口发送消息  
	if (hWnd == NULL)
	{
		(void)MessageBox(_T("获得父窗口句柄失败！"));
		return;
	}
	::SendNotifyMessage(hWnd, WM_COMMAND, MAKEWPARAM(IDC_CHECK1, BN_CLICKED), NULL);

	CDialogEx::OnClose();
}


BOOL FileDesAes_Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化      
	UpdateData(TRUE);
	((CStatic*)GetDlgItem(IDC_STATICUser))->SetWindowText(_T("自定义附加信息（字符串）"));
	CString string;
	string.Format(_T("%ld个字节"), m_Userstr.GetLength());
	((CStatic *)GetDlgItem(IDC_UserSize))->SetWindowText(string);

	((CButton*)GetDlgItem(IDC_RADIODes))->SetCheck(TRUE);
	((CEdit  *)GetDlgItem(IDC_EDIT_IV))->SetLimitText(8);  //向量最大允许输入8个字符
	((CEdit  *)GetDlgItem(IDC_EDIT_KEY))->SetLimitText(24); //秘钥最大允许输入24个字符
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


void FileDesAes_Dlg::OnBnClickedButton5()
{
	// TODO:  在此添加控件通知处理程序代码
	if (m_help_dlg == NULL)
	{
		m_help_dlg = new CHelpDlg();
		m_help_dlg->Create(IDD_HelpDlg, this);
	}
	if (m_help_dlg == NULL)
	{
		::AfxMessageBox(_T("错误"));
		return;
	}
	m_help_dlg->ShowWindow(SW_SHOWNORMAL);
}


void FileDesAes_Dlg::PostNcDestroy()
{
	// TODO:  在此添加专用代码和/或调用基类
	delete this;
	CDialogEx::PostNcDestroy();
}


void FileDesAes_Dlg::OnBnClickedButton2()
{
	// TODO:  在此添加控件通知处理程序代码
	//装载文件
	CString strReadFileName;  //文件路径
	TCHAR szFilter[] = _T("加密文件(*.mxxx)|*.mxxx|所有格式(*.*)|*.*||");  //设置文件过滤器
	CFileDialog fileDlg(TRUE, NULL, NULL, OFN_OVERWRITEPROMPT, szFilter, 0);
	fileDlg.m_ofn.lpstrInitialDir = _T("");  //设置默认打开路径
	if (IDOK == fileDlg.DoModal())  //显示窗口  
	{
		strReadFileName = fileDlg.GetPathName();   //得到文件路径
		if (m_MxxxFile.Install_File_Path(strReadFileName) == FALSE)   //装载文件 把明文数据写入缓存中
		{
			return;
		}
		UpdateData(true);
		m_Openfilepath.Format(_T(""));
		m_Openfilepath = strReadFileName;  //显示文件路径
		UpdateData(false);
	}
}


void FileDesAes_Dlg::OnBnClickedRadiodes()
{
	// TODO:  在此添加控件通知处理程序代码
	//选择了DES
	UpdateData(true);
	if (m_IVStr.GetLength() > 8)
	{
		m_IVStr = m_IVStr.Mid(0, 8);
	}
	if (m_KeyStr.GetLength() > 24)
	{
		m_KeyStr = m_KeyStr.Mid(0, 24);
	}
	UpdateData(FALSE);
	((CEdit  *)GetDlgItem(IDC_EDIT_IV))->SetLimitText(8);  //向量最大允许输入8个字符
	((CEdit  *)GetDlgItem(IDC_EDIT_KEY))->SetLimitText(24); //秘钥最大允许输入24个字符
}


void FileDesAes_Dlg::OnBnClickedRadioaes()
{
	// TODO:  在此添加控件通知处理程序代码
	((CEdit  *)GetDlgItem(IDC_EDIT_IV))->SetLimitText(16);  //向量最大允许输入16个字符
	((CEdit  *)GetDlgItem(IDC_EDIT_KEY))->SetLimitText(32); //秘钥最大允许输入32个字符
}


void FileDesAes_Dlg::OnBnClickedButton3()
{
	// TODO:  在此添加控件通知处理程序代码
	//加密保存文件
	TBYTE Keybuff[32];  //秘钥缓存
	TBYTE IvBuf[16];    //向量缓存
	bool Des_Flat = FALSE;  //是否为DES加密
	UpdateData(true);
	if (((m_Userstr.GetLength() + 4 + 4) % 16) != 0)
	{
		WORD Val = m_Userstr.GetLength() + 4 + 4;
		CString Showstr;
		if (Val>16)
		{
			Showstr.Format(_T("自定义附加数据不合法\r\n自定义数据长度+8字节必须为16的整数倍\r\n自定义数据需要再加%d个字节或者减去%d个字节"), 16 - (Val % 16), (Val % 16));
		}
		else
		{
			Showstr.Format(_T("自定义附加数据不合法\r\n自定义数据长度+8字节必须为16的整数倍\r\n自定义数据需要再加%d个字节"), 16 - (Val%16));
		}

		::AfxMessageBox(Showstr);
		return;
	}
	if (((CButton*)GetDlgItem(IDC_RADIODes))->GetCheck())  //如果当前为DES
	{
		Des_Flat = TRUE;
		if (m_IVStr.GetLength() != 8)
		{
			::AfxMessageBox(_T("向量长度必须为8字节"));
			return;
		}

		if (((m_KeyStr.GetLength() % 8) != 0) || (m_KeyStr.GetLength() == 0) || (m_KeyStr.GetLength() == 32))
		{
			::AfxMessageBox(_T("秘钥长度错误 必须为8、16、24 字节"));
			return;
		}
	}
	else   //AES
	{
		if (m_IVStr.GetLength() != 16)
		{
			::AfxMessageBox(_T("向量长度必须为16字节"));
			return;
		}

		if (((m_KeyStr.GetLength() % 16) != 0) || (m_KeyStr.GetLength() == 0))
		{
			::AfxMessageBox(_T("秘钥长度错误 必须为16、24、32 字节"));
			return;
		}
	}
	memcpy(IvBuf, m_IVStr.GetBuffer(), m_IVStr.GetLength());   //拷贝向量
	memcpy(Keybuff, m_KeyStr.GetBuffer(), m_KeyStr.GetLength()); //拷贝秘钥

	TCHAR szFilter[] = _T("加密格式(*.mxxx)|*.mxxx|所有格式(*.)|*.*||");  //设置文件过滤器
	CFileDialog fileDlg(FALSE, _T("mxxx"), NULL, OFN_OVERWRITEPROMPT, szFilter, 0); //默认扩展名为Bin
	fileDlg.m_ofn.lpstrInitialDir = _T("");  //设置默认打开路径
	if (IDOK == fileDlg.DoModal())  //显示窗口
	{
		CString strReadFileName;  //文件路径
		strReadFileName = fileDlg.GetPathName();  //得到文件路径
		if (m_MxxxFile.Install_File_Path(m_Openfilepath) == FALSE)   //装载文件 把明文数据写入缓存中
		{
			return;
		}
		if (m_MxxxFile.File_Encrypt_exe(m_Userstr, IvBuf, Keybuff, m_KeyStr.GetLength(), Des_Flat) == false) //执行文件加密
		{
			return;
		}
		if (m_MxxxFile.Save_EncryptFile(strReadFileName) == false) //创建加密文件 保存密文数据
		{
			return;
		}
		m_EnFilePath.Format(_T(""));
		m_EnFilePath = strReadFileName;  //显示文件路径
		UpdateData(false);
	}
}


void FileDesAes_Dlg::OnEnChangeEdit9()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。
	UpdateData(true);
	CString str;
	str.Format(_T("%ld个字节"), m_Userstr.GetLength());
	((CStatic *)GetDlgItem(IDC_UserSize))->SetWindowText(str);
	UpdateData(false);
	// TODO:  在此添加控件通知处理程序代码
}


void FileDesAes_Dlg::OnBnClickedButton4()
{
	// TODO:  在此添加控件通知处理程序代码
	//解密保存文件
	TBYTE Keybuff[32];  //秘钥缓存
	TBYTE IvBuf[16];    //向量缓存
	bool Des_Flat = FALSE;  //是否为DES加密
	UpdateData(true);
	if (((CButton*)GetDlgItem(IDC_RADIODes))->GetCheck())  //如果当前为DES
	{
		Des_Flat = TRUE;
		if (m_IVStr.GetLength() != 8)
		{
			::AfxMessageBox(_T("向量长度必须为8字节"));
			return;
		}

		if (((m_KeyStr.GetLength() % 8) != 0) || (m_KeyStr.GetLength() == 0) || (m_KeyStr.GetLength() == 32))
		{
			::AfxMessageBox(_T("秘钥长度错误 必须为8、16、24 字节"));
			return;
		}
	}
	else   //AES
	{
		if (m_IVStr.GetLength() != 16)
		{
			::AfxMessageBox(_T("向量长度必须为16字节"));
			return;
		}

		if (((m_KeyStr.GetLength() % 16) != 0) || (m_KeyStr.GetLength() == 0))
		{
			::AfxMessageBox(_T("秘钥长度错误 必须为16、24、32 字节"));
			return;
		}
	}
	memcpy(IvBuf, m_IVStr.GetBuffer(), m_IVStr.GetLength());   //拷贝向量
	memcpy(Keybuff, m_KeyStr.GetBuffer(), m_KeyStr.GetLength()); //拷贝秘钥

	TCHAR szFilter[] = _T("所有格式(*.)|*.*||");  //设置文件过滤器
	CFileDialog fileDlg(FALSE, NULL, NULL, OFN_OVERWRITEPROMPT, szFilter, 0); //默认扩展名为Bin
	fileDlg.m_ofn.lpstrInitialDir = _T("");  //设置默认打开路径
	if (IDOK == fileDlg.DoModal())  //显示窗口
	{
		CString strReadFileName;  //文件路径
		CString userstr; //解密后的用户自定义数据
		strReadFileName = fileDlg.GetPathName();  //得到文件路径
		if (m_MxxxFile.Install_File_Path(m_Openfilepath) == FALSE)   //装载文件 把明文数据写入缓存中
		{
			return;
		}
		if (m_MxxxFile.File_Decode_exe(&userstr, IvBuf, Keybuff, m_KeyStr.GetLength(), Des_Flat) == false) //执行文件解密
		{
			return;
		}
		if (m_MxxxFile.Save_EncryptFile(strReadFileName) == false) //创建解密文件 保存解密数据数据
		{
			return;
		}
		if (userstr != _T(""))
		{
			m_Userstr = userstr;
			userstr.Format(_T("%4d个字节"), m_Userstr.GetLength());
			((CStatic *)GetDlgItem(IDC_UserSize))->SetWindowText(userstr);
		}
		m_Defilepath.Format(_T(""));
		m_Defilepath = strReadFileName;  //显示文件路径
		UpdateData(false);
	}
}
