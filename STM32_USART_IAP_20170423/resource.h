//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 STM32_USART_IAP_20170423.rc 使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_STM32_USART_IAP_20170423_DIALOG 102
#define IDR_MAINFRAME                   128
#define IDD_FileDesAes                  130
#define IDD_HelpDlg                     132
#define IDC_COMBO1                      1001
#define IDC_COMBO2                      1002
#define IDC_EDIT1                       1004
#define IDC_BUTTON2                     1005
#define IDC_PROGRESS1                   1006
#define IDC_BUTTON3                     1006
#define IDC_BUTTON4                     1007
#define IDC_BUTTON5                     1009
#define IDC_STATIC_JD                   1011
#define IDC_BUTTON1                     1012
#define IDC_EDIT2                       1013
#define IDC_CHECK1                      1014
#define IDC_EDIT5                       1014
#define IDC_EDIT_IV                     1015
#define IDC_EDIT_KEY                    1016
#define IDC_RADIOAes                    1017
#define IDC_RADIODes                    1018
#define IDC_EDIT6                       1019
#define IDC_EDIT9                       1022
#define IDC_STATICUser                  1023
#define IDC_UserSize                    1024

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1025
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
