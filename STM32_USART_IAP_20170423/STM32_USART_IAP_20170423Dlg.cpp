
// STM32_USART_IAP_20170423Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "STM32_USART_IAP_20170423.h"
#include "STM32_USART_IAP_20170423Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "My_seriaPort.h"
#include "Usart_ISP.h"
#pragma   comment(lib, "AES_DLL.lib ") 
#pragma   comment(lib, "DES_DLL.lib ") 
//升级线程指针 
CWinThread  * pupMCUThread = NULL;
//退出升级线程标记
bool upMCUThread_Exit = FALSE;

//自定义串口ISP 类
Cusart_isp  mCusart_isp;
// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CSTM32_USART_IAP_20170423Dlg 对话框



CSTM32_USART_IAP_20170423Dlg::CSTM32_USART_IAP_20170423Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSTM32_USART_IAP_20170423Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSTM32_USART_IAP_20170423Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSTM32_USART_IAP_20170423Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_CBN_DROPDOWN(IDC_COMBO1, &CSTM32_USART_IAP_20170423Dlg::OnCbnDropdownCombo1)
	ON_BN_CLICKED(IDC_BUTTON2, &CSTM32_USART_IAP_20170423Dlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON5, &CSTM32_USART_IAP_20170423Dlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON1, &CSTM32_USART_IAP_20170423Dlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_CHECK1, &CSTM32_USART_IAP_20170423Dlg::OnBnClickedCheck1)
END_MESSAGE_MAP()


const  char * Cboaudtalb[] = { "4800", "9600", "115200" };


// CSTM32_USART_IAP_20170423Dlg 消息处理程序

BOOL CSTM32_USART_IAP_20170423Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO:  在此添加额外的初始化代码
	//	SendMessage(WM_COMMAND, MAKEWPARAM(IDC_COMBO1, CBN_DROPDOWN), NULL);  //发送一个消息 虚拟成按键被单击
	OnCbnDropdownCombo1();
	((CComboBox *)GetDlgItem(IDC_COMBO2))->ResetContent();
	//添加波特率值
	for (size_t i = 0; i < (sizeof(Cboaudtalb) / sizeof(Cboaudtalb[0])); i++)
	{
		((CComboBox *)GetDlgItem(IDC_COMBO2))->AddString((LPCTSTR)Cboaudtalb[i]);
	}
	((CComboBox *)GetDlgItem(IDC_COMBO2))->SetCurSel(0); //选择波特率
	((CEdit *)GetDlgItem(IDC_EDIT2))->SetLimitText(10);  //设置文本输入 最大10个字符
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CSTM32_USART_IAP_20170423Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CSTM32_USART_IAP_20170423Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CSTM32_USART_IAP_20170423Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CSTM32_USART_IAP_20170423Dlg::OnCbnDropdownCombo1()
{
	// TODO:  在此添加控件通知处理程序代码
	((CComboBox *)GetDlgItem(IDC_COMBO1))->ResetContent();
	UpdateData(false);
	if (mCusart_isp.Scan_CSerialPortCom()) //扫描串口号
	{
		WORD Count = mCusart_isp.m_Name.GetCount(); //得到串口数量
		for (size_t i = 0; i < Count; i++)
		{
			((CComboBox *)GetDlgItem(IDC_COMBO1))->AddString(mCusart_isp.m_Name[i]);
		}
		((CComboBox *)GetDlgItem(IDC_COMBO1))->SetCurSel(0);
	}
}

void CSTM32_USART_IAP_20170423Dlg::OnBnClickedButton2()
{
	// TODO:  在此添加控件通知处理程序代码
	CString strReadFileName;  //文件路径
	//CString strExe("bin");
	//CString strOpenFileName("*.bin");
	//TCHAR szFilter[] = _T("(*.hex)|*.hex|(*.bin)|*.bin|所有文件(*.*)|*.*||");  //设置文件过滤器
	//CFileDialog fileDlg(TRUE, strExe, strOpenFileName, OFN_OVERWRITEPROMPT, szFilter, 0);
	TCHAR szFilter[] = _T("文件格式hex、bin(*.hex;*.bin)|*.hex;*.bin||");  //设置文件过滤器
	CFileDialog fileDlg(TRUE, NULL, NULL, OFN_OVERWRITEPROMPT, szFilter, 0);
	fileDlg.m_ofn.lpstrInitialDir = _T("");  //设置默认打开路径
	if (IDOK == fileDlg.DoModal())  //显示窗口
	{
		strReadFileName = fileDlg.GetPathName();
		if (mCusart_isp.Install_Upfile_path(strReadFileName))
		{
			SetDlgItemText(IDC_EDIT1, strReadFileName); //显示文件路径

			CString string;
			string = strReadFileName.Mid(strReadFileName.ReverseFind('.'));  //反相找寻. 找到文件格式
			string.MakeUpper(); //改为大写
			if (string == _T(".HEX"))
			{
				//把地址转换成字符串
				//char buff[20];
				//_ltoa_s(mCusart_isp.m_Upflashaddr, (char *)buff, 20, 16);
				strReadFileName.Format(_T("0X%08X"), mCusart_isp.m_Upflashaddr);
				SetDlgItemText(IDC_EDIT2, strReadFileName);  //显示地址
				((CEdit *)GetDlgItem(IDC_EDIT2))->SetReadOnly(TRUE);  //设置为只读
			}
			else if (string == _T(".BIN"))
			{
				((CEdit *)GetDlgItem(IDC_EDIT2))->SetReadOnly(FALSE);  //设置关闭只读
			}
		}
	}
}


UINT UpmcuThread(LPVOID pParam)
{
	CSTM32_USART_IAP_20170423Dlg *CSTM32_USART = (CSTM32_USART_IAP_20170423Dlg *)pParam;
	if (mCusart_isp.Updata_file_exe()) //执行升级流程
	{
		::AfxMessageBox(_T("升级成功"));
	}
	else
	{
		//	::AfxMessageBox("升级失败");
	}
	TRACE(_T("\r\n 升级线程退出\r\n"));
	mCusart_isp.Close_CSerialPortCom();
	upMCUThread_Exit = false;  //标记线程退出
	return 0;
}

void CSTM32_USART_IAP_20170423Dlg::OnBnClickedButton5()
{
	// TODO:  在此添加控件通知处理程序代码
	if (upMCUThread_Exit == false)
	{
		mCusart_isp.m_Prog = (CProgressCtrl *)GetDlgItem(IDC_PROGRESS1); //装载进度条
		mCusart_isp.m_Progtxt = (CStatic *)GetDlgItem(IDC_STATIC_JD);//状态进度条显示文本
		CString strcom;
		CString strbaud;
		GetDlgItemText(IDC_EDIT2, strbaud);  //得到地址字符串
		GetDlgItemText(IDC_EDIT1, strcom); //得到文件路径
		strcom.MakeUpper(); //转换成大写
		if (strcom.Mid(strcom.GetLength() - 4) == _T(".BIN"))  //如果当前为bin文件
		{
			if (strbaud == _T(""))
			{
				::AfxMessageBox(_T("输入地址无效"));
				return;
			}
			strbaud.MakeUpper(); //转换成大小
			if ((strbaud[1] == 'X') && (strbaud[0] != '0'))
			{
				::AfxMessageBox(_T("地址输入格式不对"));
				return;  //不为HEX格式
			}
			for (int i = 0; i < strbaud.GetLength(); i++)  //检查
			{
				if (::isxdigit(strbaud[i]) == false)  //检查是否为十六进制数
				{
					if ((i == 1) && (strbaud[i] == 'X'))
					{
						continue;
					}
					::AfxMessageBox(_T("请输入十六进制数"));
					return;  //不为HEX格式
				}
			}
			mCusart_isp.m_Upflashaddr = strtol((const char *)strbaud.GetBuffer(), NULL, 16); //把地址字符串转换成数值
		}
		GetDlgItemText(IDC_EDIT1, strcom); //得到文件路径
		if (mCusart_isp.Install_Upfile_path(strcom) == false)
		{
			//	::AfxMessageBox(_T("打开文件失败"));
			return;
		}
		GetDlgItemText(IDC_COMBO1, strcom);  //得到串口号
		GetDlgItemText(IDC_COMBO2, strbaud);  //得到波特率
		if (mCusart_isp.Open_CSerialPortCom(strcom, atoi((const char *)strbaud.GetBuffer()), EVENPARITY) == false) //打开串口
		{
			::AfxMessageBox(_T("打开串口失败"));
			return;
		}
		pupMCUThread = ::AfxBeginThread(UpmcuThread, this); //开启线程
		if (pupMCUThread != NULL)
		{
			upMCUThread_Exit = true;
			TRACE(_T("\r\n 开启升级线程\r\n"));
		}
	}
}





void CSTM32_USART_IAP_20170423Dlg::OnBnClickedButton1()
{
	// TODO:  在此添加控件通知处理程序代码
	CString Hexpath;  //HEX文件路径
	GetDlgItemText(IDC_EDIT1, Hexpath); //得到文件路径
	if (mCusart_isp.Install_Upfile_path(Hexpath) == false)  //装载文件
	{
		return;
	}

	TCHAR szFilter[] = _T("文件格式bin(*.bin)|*.bin||");  //设置文件过滤器
	CFileDialog fileDlg(FALSE, _T("BIN"), NULL, OFN_OVERWRITEPROMPT, szFilter, 0); //默认扩展名为Bin
	fileDlg.m_ofn.lpstrInitialDir = _T("");  //设置默认打开路径
	if (IDOK == fileDlg.DoModal())  //显示窗口
	{
		CString strReadFileName;  //文件路径
		strReadFileName = fileDlg.GetPathName();  //得到文件路径
		CStdioFile  file;
		file.Open(strReadFileName, CFile::modeCreate | CFile::modeWrite | CStdioFile::typeBinary);  //创建文件
		TBYTE *bindata = new TBYTE[mCusart_isp.GetFileDataCount(NULL)];   //申请缓存
		mCusart_isp.GetFileDataCount(bindata);  //得到Bin文件数据
		file.Write(bindata, mCusart_isp.GetFileDataCount(NULL));
		file.Close();
		delete bindata;
		::AfxMessageBox(_T("保存成功"));
	}
}


void CSTM32_USART_IAP_20170423Dlg::OnBnClickedCheck1()
{
	// TODO:  在此添加控件通知处理程序代码
	static bool Dlg_Flat = FALSE;  //窗口显示状态 
	if (FAesDes_dlg == NULL)
	{
		FAesDes_dlg = new FileDesAes_Dlg();
		FAesDes_dlg->Create(IDD_FileDesAes, this);
		FAesDes_dlg->ShowWindow(SW_HIDE);  //隐藏窗口
		//	FAesDes_dlg->DoModal();
		Dlg_Flat = FALSE;
	}
	if (FAesDes_dlg == NULL)
	{
		::AfxMessageBox(_T("子窗口的句柄为空！ "));
		return;
	}

	if (Dlg_Flat != FALSE)  //如果当前是显示
	{
		FAesDes_dlg->SendMessage(WM_CLOSE);
		Dlg_Flat = FALSE; //标记窗口关闭 
		((CButton *)GetDlgItem(IDC_CHECK1))->SetCheck(FALSE);
	}
	else
	{
		FAesDes_dlg->ShowWindow(SW_SHOWNORMAL);  //显示窗口
		Dlg_Flat = TRUE;
	}
}



