#include "stdafx.h"
#include "My_seriaPort.h"
#include <process.h>  
#include <iostream>  
#include "atlbase.h"

//线程退出标记
bool CSerialPortCom::m_bSetReceThreadexit = FALSE;

CSerialPortCom::CSerialPortCom(void)
{
	m_hPortCom = INVALID_HANDLE_VALUE;
	m_hPortCom_ReceThread = NULL;
	InitializeCriticalSection(&m_csCommunicationSync);   //创建互斥信号量
	m_recbufCount = 0; //指针偏移清零
	memset(m_recbuf, 0, CSerialPortCom_MaxBuf);  //接收缓存清零
}

CSerialPortCom::~CSerialPortCom(void)
{
	m_Name.RemoveAll(); //删除所有数据
	m_Name.FreeExtra(); //释放内存
	Close_CSerialPortCom();  //关闭打开的串口及线程
	DeleteCriticalSection(&m_csCommunicationSync);  //删除互斥信号量
}

void CSerialPortCom::Close_CSerialPortCom(void)
{
	if ((m_hPortCom_ReceThread != NULL) || (m_bSetReceThreadexit == TRUE)) //如果线程开启了
	{
		m_bSetReceThreadexit = FALSE; //设置线程退出
		TRACE("设置线程退出");
		Sleep(50);
		m_hPortCom_ReceThread = NULL;
	}
	if (m_hPortCom != INVALID_HANDLE_VALUE)
	{
		CloseHandle(m_hPortCom);
		m_hPortCom = INVALID_HANDLE_VALUE;
	}
}
int CSerialPortCom::Scan_CSerialPortCom(void)
{
	TCHAR Name[255];
	UCHAR szPortName[255];
	LONG  Status;
	DWORD dwIndex = 0;
	DWORD dwName;
	DWORD dwSizeofPortName;
	DWORD Type;
	HKEY hKey;

	//TRACE
	LPCTSTR data_Set = _T("HARDWARE\\DEVICEMAP\\SERIALCOMM\\");
	m_Name.RemoveAll(); //清除所以数组数据
	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, data_Set, 0, KEY_READ, &hKey) == ERROR_SUCCESS) //打开一个制定的注册表键,成功返回ERROR_SUCCESS即“0”值
	{
		do
		{
			//每读取一次dwName和dwSizeofPortName都会被修改 
			//注意一定要重置,否则会出现很离奇的错误,本人就试过因没有重置,出现先插入串口号大的（如COM4）,再插入串口号小的（如COM3），此时虽能发现两个串口，但都是同一串口号（COM4）的问题，同时也读不了COM大于10以上的串口 
			dwName = sizeof(Name);
			dwSizeofPortName = sizeof(szPortName);

			Status = RegEnumValue(hKey, dwIndex, Name, &dwName, NULL, &Type, szPortName, &dwSizeofPortName);//读取键值 
			if ((Status == ERROR_SUCCESS) || (Status == ERROR_MORE_DATA))
			{
				dwIndex++;   //下一个索引
				m_Name.Add(CString(szPortName)); // 串口字符串保存 
				TRACE("serial:%s\n", szPortName);  //打印串口号
				TRACE("Name:%s\n", Name);  //打印名称
			}
		} while ((Status == ERROR_SUCCESS) || (Status == ERROR_MORE_DATA));
		RegCloseKey(hKey);
	}
	return m_Name.GetCount();
}

bool CSerialPortCom::Open_CSerialPortCom(LPCTSTR COMX, int BaudRate, int Parity, int StopBits)
{
	DCB dcb = { 0 };
	if (IsPortComOpen() == true)  //如果之前串口已经打开了
	{
		Close_CSerialPortCom();  //关闭串口
		TRACE("\r\n在打开串口的调节下重新打开串口");
	}

	// 在COM口大于9时需要在前面加“\\\\.\\”否则创建不了文件 比如com17需要表示为\\\\.\\com17
	//测试发现当COM口为1至9时，加“\\\\.\\”也可以创建文件
	m_hPortCom = CreateFile(
		_T("\\\\.\\") + CString(COMX),  //要打开的串口名
		GENERIC_READ | GENERIC_WRITE,  //串口的访问类型为读写  
		0,  //独占方式，串口不能共享
		NULL,
		OPEN_EXISTING,  //打开而非创建 
		//FILE_ATTRIBUTE_NORMAL|FILE_FLAG_OVERLAPPED,  //异步重叠方式
		NULL, //同步方式
		NULL
		);

	if (m_hPortCom == INVALID_HANDLE_VALUE)   //如果无效
	{
		DWORD dwError = GetLastError();
		return FALSE;
	}

	dcb.DCBlength = sizeof(DCB);

	if (!GetCommState(m_hPortCom, &dcb))  //先Get DCB对象 
	{
		DWORD dwError = GetLastError();
		return FALSE;
	}

	//填充DCB对象 设置串口传输的参数
	dcb.BaudRate = BaudRate;    //波特率  
	dcb.ByteSize = 8;           //通信字节数
	dcb.Parity = Parity;      //无奇偶检验  
	dcb.StopBits = StopBits;   //1个停止位数  

	if (!SetCommState(m_hPortCom, &dcb))  //再Set DCB对象  
	{
		DWORD dwError = GetLastError();
		return FALSE;
	}
	COMMTIMEOUTS   ComTimeOut;
	ComTimeOut.ReadIntervalTimeout = 0;         //读间隔超时
	ComTimeOut.ReadTotalTimeoutMultiplier = 0;  //读时间系数
	ComTimeOut.ReadTotalTimeoutConstant = 0;    //读时间常量
	ComTimeOut.WriteTotalTimeoutMultiplier = 0; //写时间系数
	ComTimeOut.WriteTotalTimeoutConstant = 0;   //写时间常量
	SetCommTimeouts(m_hPortCom, &ComTimeOut);

	if (!PurgeComm(m_hPortCom, PURGE_TXCLEAR | PURGE_RXCLEAR))  //读写之前要先调用PurgeComm清空接收缓冲区   清除输出缓冲区
	{
		return FALSE;
	}
	SetupComm(m_hPortCom, CSerialPortCom_MaxBuf, CSerialPortCom_MaxBuf);  //启动串口并设置输入/输出缓冲区的大小均为4096
	m_bSetReceThreadexit = TRUE; //标记线程开启
	m_hPortCom_ReceThread = ::AfxBeginThread(CSerialPortCom::PortCom_ReceThread, (LPVOID)this);
	if (m_hPortCom_ReceThread != NULL)
	{
		TRACE("\r\n接收线程开启成功!\r\n");
	}
	else
	{
		m_bSetReceThreadexit = FALSE;
		Close_CSerialPortCom(); //关闭线程 及关闭串口句柄
		TRACE("\r\n接收线程开启失败!\r\n");
		return FALSE;
	}
	return TRUE;
}

UINT CSerialPortCom::PortCom_ReceThread(LPVOID pParam)
{
	UINT Count = 0;
	TBYTE Usart_Data = 0;
	CSerialPortCom *pCSerialPortCom = (CSerialPortCom *)pParam;
	while (pCSerialPortCom->m_bSetReceThreadexit == TRUE)
	{
		if (pCSerialPortCom->m_hPortCom != INVALID_HANDLE_VALUE)  //当前串口句柄有效
		{
			Count = pCSerialPortCom->GetBytesInCOM();  //得到当前串口缓存接收数据数量
			if ((Count == 0) || (Count == -1))
			{
				Sleep(5);
				continue;
			}
			TRACE(_T("\r\n"));
			//采用环形BUFF来接收串口数据
			do
			{
				//接收一个字节数据   并放入指定偏移缓存地址中
				if (pCSerialPortCom->ReadChar(pCSerialPortCom->m_recbuf[pCSerialPortCom->m_recbufCount]) == TRUE)
				{
					pCSerialPortCom->m_recbufCount++;  //缓存地址偏移
					//构成环形BUFF
					if (pCSerialPortCom->m_recbufCount >= CSerialPortCom_MaxBuf)
					{
						pCSerialPortCom->m_recbufCount = 0;
					}
				}
				else
				{
					//关闭串口
					pCSerialPortCom->Close_CSerialPortCom();
					break;  //跳出
				}
			} while (--Count);
		}
		else
		{
			Sleep(5);
		}
	}
	TRACE("\r\n线程退出\r\n");
	return 0;
}

//返回串口接收缓存数据数量
int CSerialPortCom::GetBytesInCOM()
{
	DWORD dwError = 0;  /** 错误码 */
	COMSTAT  comstat;   /** COMSTAT结构体,记录通信设备的状态信息 */
	memset(&comstat, 0, sizeof(COMSTAT));

	UINT BytesInQue = 0;
	/** 在调用ReadFile和WriteFile之前,通过本函数清除以前遗留的错误标志 */
	if (ClearCommError(m_hPortCom, &dwError, &comstat))
	{
		BytesInQue = comstat.cbInQue; /** 获取在输入缓冲区中的字节数 */
	}
	else
	{
		//关闭串口
		Close_CSerialPortCom();
		return -1;
	}
	return BytesInQue;
}

//从串口读一个字节
bool CSerialPortCom::ReadChar(unsigned char & cRecved)
{
	BOOL  bResult = TRUE;
	DWORD BytesRead = 0;
	if (m_hPortCom == INVALID_HANDLE_VALUE)
	{
		return false;
	}
	/** 临界区保护 */
	EnterCriticalSection(&m_csCommunicationSync);

	/** 从缓冲区读取一个字节的数据 */
	bResult = ReadFile(m_hPortCom, &cRecved, 1, &BytesRead, NULL);
	if (bResult == false)
	{
		/** 获取错误码,可以根据该错误码查出错误原因 */
		DWORD dwError = GetLastError();

		/** 清空串口缓冲区 */
		PurgeComm(m_hPortCom, PURGE_RXCLEAR | PURGE_RXABORT);
		/** 离开临界区 */
		LeaveCriticalSection(&m_csCommunicationSync);

		return false;
	}

	/** 离开临界区 */
	LeaveCriticalSection(&m_csCommunicationSync);

	return (BytesRead == 1);
}
//读指定长度数据
bool CSerialPortCom::ReadData(unsigned char * pData, DWORD length)
{
	BOOL  bResult = TRUE;
	DWORD BytesRead = 0;
	if (m_hPortCom == INVALID_HANDLE_VALUE)
	{
		return false;
	}
	/** 临界区保护 */
	EnterCriticalSection(&m_csCommunicationSync);

	/** 从缓冲区读取一个字节的数据 */
	bResult = ReadFile(m_hPortCom, pData, length, &BytesRead, NULL);
	if (bResult == false)
	{
		/** 获取错误码,可以根据该错误码查出错误原因 */
		DWORD dwError = GetLastError();

		/** 清空串口缓冲区 */
		PurgeComm(m_hPortCom, PURGE_RXCLEAR | PURGE_RXABORT);
		/** 离开临界区 */
		LeaveCriticalSection(&m_csCommunicationSync);

		return false;
	}

	/** 离开临界区 */
	LeaveCriticalSection(&m_csCommunicationSync);

	return TRUE;
}
bool CSerialPortCom::WriteData(LPVOID pData, DWORD length)
{
	BOOL   bResult = TRUE;
	DWORD  BytesToSend = 0;
	if (m_hPortCom == INVALID_HANDLE_VALUE)
	{
		return false;
	}
	/** 临界区保护 */
	EnterCriticalSection(&m_csCommunicationSync);

	/** 向缓冲区写入指定量的数据 */
	bResult = WriteFile(m_hPortCom, pData, length, &BytesToSend, NULL);
	if (!bResult)  //写出错
	{
		DWORD dwError = GetLastError();
		/** 清空串口缓冲区 */
		PurgeComm(m_hPortCom, PURGE_RXCLEAR | PURGE_RXABORT);
		LeaveCriticalSection(&m_csCommunicationSync);
		return false;
	}

	/** 离开临界区 */
	LeaveCriticalSection(&m_csCommunicationSync);

	return true;
}

//返回当前串口是否打开  0关闭   1打开
bool CSerialPortCom::IsPortComOpen(void)
{
	if (m_hPortCom != INVALID_HANDLE_VALUE)
	{
		return true;
	}
	return false;
}
bool CSerialPortCom::Set_EscapeCommFunction(DWORD dwFunc)
{
	if (IsPortComOpen() == TRUE)
	{
		return EscapeCommFunction(m_hPortCom, dwFunc) ? true : false;
	}
	return false;
}
