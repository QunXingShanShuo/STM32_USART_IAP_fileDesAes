#ifndef  _MY_SERIAPORT_H_
#define  _MY_SERIAPORT_H_
#include <Windows.h>  
#include "afxtempl.h"


/** 串口通信类
*
*  本类实现了对串口的基本操作
*  例如监听发到指定串口的数据、发送指定数据到串口
*/
class CSerialPortCom
{
public:
	CSerialPortCom(void);
	~CSerialPortCom(void);

public:
	//串口接收 发送缓存大小
#define CSerialPortCom_MaxBuf     4096
	//扫描的串口名
	CArray<CString, CString&> m_Name;
	//接收数据缓存指针
	WORD m_recbufCount;
	//接收数据缓存
	BYTE   m_recbuf[CSerialPortCom_MaxBuf];
	//扫描当前PC上的端口  返回串口数 
	int Scan_CSerialPortCom();
	/*
	函数名称：打开串口
	入口参数：COMx 串口号 _T("COM2")
	BaudRate  波特率
	Parity    奇偶校验 NOPARITY  ODDPARITY 奇   EVENPARITY 偶 
	StopBits  停止位  ONESTOPBIT 1个停止位
	返回值：  0   成功
	-1  失败  无效的句柄
	*/
	bool Open_CSerialPortCom(LPCTSTR COMX, int BaudRate, int Parity = NOPARITY /*奇偶校验*/, int StopBits = ONESTOPBIT/*停止位*/);
	//关闭串口
	void Close_CSerialPortCom(void);
	//当前串口是否打开   0关闭   1打开
	bool IsPortComOpen(void);
	//发送数据
	bool WriteData(LPVOID pData, DWORD length);
	//设置串口RTS DTR 引脚
	bool Set_EscapeCommFunction(DWORD dwFunc);

private:
	//返回串口接收缓存数据
	int GetBytesInCOM();
	//从串口读一个字节
	bool ReadChar(unsigned char & cRecved);
	//读指定长度数据
	bool ReadData(unsigned char * pData, DWORD length);
	/** 串口监听线程
	*
	*  监听来自串口的数据和信息
	*  @param:  void * pParam 线程参数
	*  @return: UINT WINAPI 线程返回值
	*  @note:
	*  @see:
	*/
	static UINT  PortCom_ReceThread(LPVOID pParam);
private:

	/** 串口句柄 */
	HANDLE  m_hPortCom;
	//接收线程指针 
	CWinThread  * m_hPortCom_ReceThread;
	/** 线程退出标志变量 */
	static bool m_bSetReceThreadexit;
	/** 同步互斥,临界区保护 */
	CRITICAL_SECTION   m_csCommunicationSync;       //!< 互斥操作串口  

};
























#endif