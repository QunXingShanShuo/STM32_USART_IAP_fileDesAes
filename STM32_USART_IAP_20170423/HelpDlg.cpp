// HelpDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "STM32_USART_IAP_20170423.h"
#include "HelpDlg.h"
#include "afxdialogex.h"


// CHelpDlg 对话框

IMPLEMENT_DYNAMIC(CHelpDlg, CDialogEx)

CHelpDlg::CHelpDlg(CWnd* pParent /*=NULL*/)
: CDialogEx(CHelpDlg::IDD, pParent)
, m_Helpstr(_T(""))
{

}

CHelpDlg::~CHelpDlg()
{
}

void CHelpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_Helpstr);
}


BEGIN_MESSAGE_MAP(CHelpDlg, CDialogEx)
END_MESSAGE_MAP()


// CHelpDlg 消息处理程序


BOOL CHelpDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化    
	UpdateData(TRUE);
	m_Helpstr += _T("一、文件加密支持单DES、双DES、三DES、128bitAES、192bitAES、256bitAES\r\n");
	m_Helpstr += _T("二、当为AES加密时，向量为16字节,当为DES时，向量为8字节。加密模式都为CBC\r\n");
	m_Helpstr += _T("三、会根据您输入的秘钥长度自动执行对应的加密及解密\r\n");
	m_Helpstr += _T("四、只支持字符串输入\r\n");
	m_Helpstr += _T("五、加密后文件格式为 4字节自定义数据长度+自定义数据+4字节明文长度+明文数据\r\n");
	m_Helpstr += _T("六、加密后的文件格式为.mxxx\r\n"); 
	m_Helpstr += _T("七、自定义数据长度 + 8字节必须为16的整数倍\r\n");
	m_Helpstr += _T("八、如果密文中有自定义数据则解密后会在文本框中显示\r\n");
	m_Helpstr += _T("九、加密、解密都只对当前打开的文件操作\r\n");
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


void CHelpDlg::PostNcDestroy()
{
	// TODO:  在此添加专用代码和/或调用基类
	delete this;
	CDialogEx::PostNcDestroy();
}
