#pragma once  //关键字 防止重复包含

// 下列 ifdef 块是创建使从 DLL 导出更简单的
// 宏的标准方法。此 DLL 中的所有文件都是用命令行上定义的 AES_DLL_EXPORTS
// 符号编译的。在使用此 DLL 的
// 任何其他项目上不应定义此符号。这样，源文件中包含此文件的任何其他项目都会将
// AES_DLL_API 函数视为是从 DLL 导入的，而此 DLL 则将用此宏定义的
// 符号视为是被导出的。
#ifdef AES_DLL_EXPORTS
#define AES_DLL_API __declspec(dllexport)
#else
#define AES_DLL_API __declspec(dllimport)
#endif

//AES加密  16字节一个数据块
//IV_IN_OUT:        向量输入  密文输出
//State_IN_OUT：    明文输入  密文输出
//key128bit:        秘钥  128bit  16字节
AES_DLL_API void Aes_IV_key128bit_Encrypt(unsigned char *IV_IN_OUT, unsigned char *State_IN_OUT, unsigned char *key128bit);


//AES解密  16字节一个数据块
//IV_IN_OUT:        向量输入  原密文输出
//State_IN_OUT：    密文输入  明文输出
//key128bit:        秘钥  128bit  16字节
AES_DLL_API void Aes_IV_key128bit_Decode(unsigned char *IV_IN_OUT, unsigned char *State_IN_OUT, unsigned char *key128bit);

//AES加密  16字节一个数据块
//IV_IN_OUT:        向量输入  密文输出
//State_IN_OUT：    明文输入  密文输出
//key192bit:        秘钥  192bit  24字节
AES_DLL_API void Aes_IV_key192bit_Encrypt(unsigned char *IV_IN_OUT, unsigned char *State_IN_OUT, unsigned char *key192bit);

//AES解密  16字节一个数据块
//IV_IN_OUT:        向量输入  原密文输出
//State_IN_OUT：    密文输入  明文输出
//key192bit:        秘钥  192bit  24字节
AES_DLL_API void Aes_IV_key192bit_Decode(unsigned char *IV_IN_OUT, unsigned char *State_IN_OUT, unsigned char *key192bit);


//AES加密  16字节一个数据块
//IV_IN_OUT:        向量输入  密文输出
//State_IN_OUT：    明文输入  密文输出
//key256bit:        秘钥  256bit  32字节
AES_DLL_API void Aes_IV_key256bit_Encrypt(unsigned char *IV_IN_OUT, unsigned char *State_IN_OUT, unsigned char *key256bit);


//AES解密  16字节一个数据块
//IV_IN_OUT:        向量输入  原密文输出
//State_IN_OUT：    密文输入  明文输出
//key256bit:        秘钥  256bit  32字节
AES_DLL_API void Aes_IV_key256bit_Decode(unsigned char *IV_IN_OUT, unsigned char *State_IN_OUT, unsigned char *key256bit);


