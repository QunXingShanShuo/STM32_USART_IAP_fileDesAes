#include "stdafx.h"
#include "Usart_ISP.h"
#include <shlwapi.h>

Cusart_isp::Cusart_isp(void)
{
	Upfsize = 0;
	Upfsizeoff = 0;
	m_Bootloader_Ver = 0;
	m_Mcu_PID = 0;
	m_Prog = NULL;
	m_Progtxt = NULL;
	m_Upflashaddr = DefFlashAddr; //FLASH的起始地址
}
Cusart_isp::~Cusart_isp(void)
{
	Upfdata.RemoveAll();
	Upfdata.FreeExtra();
	m_Prog = NULL;
	m_Progtxt = NULL;
}

bool Cusart_isp::Install_Upfile_path(LPCTSTR path)
{
	CString filepath(path);  //文件路径
	CString filetyp;
	int index = 0;
	Upfdata.RemoveAll();  //删除所有数据
	Upfdata.FreeExtra();  //执行
	if (::PathFileExists(path) == false) //检查文件或者文件夹是否存在
	{
		::AfxMessageBox(_T("文件不存在"));
		return false;
	};
	index = filepath.ReverseFind('.');  //逆向找寻 '.'
	filetyp = filepath.Mid(index);  //得到文件类型
	filetyp.MakeUpper(); //转换成大写
	if (filetyp == ".BIN")
	{
		return Install_Bin_Upfile_path(filepath);
	}
	else if (filetyp == ".HEX")
	{
		return Install_Hex_Upfile_path(filepath);
	}

	//其它类型的文件全案BIN文件处理
	return Install_Bin_Upfile_path(filepath);
	//return true;
}

//导入bin升级文件
bool Cusart_isp::Install_Bin_Upfile_path(LPCTSTR path)
{
	CFile Upfile;
	if (Upfile.Open(path, CFile::modeRead))   //打开文件
	{
		Upfdata.RemoveAll();  //删除所有数据
		Upfdata.FreeExtra(); //执行
		Upfsize = Upfile.GetLength();  //得到文件大小
		Upfdata.SetSize((INT_PTR)Upfsize); //设置大小
		TBYTE * buff = new TBYTE[Upfsize]; //开辟缓存
		Upfile.SeekToBegin(); //设置读写指针到文件头
		Upfile.Read(buff, Upfsize);//读取全部文件数据
		Upfile.Close();
		for (size_t i = 0; i < Upfsize; i++)  //把文件数据加入数组
		{
			Upfdata[i] = buff[i];
		}
		Upfpath.Format(_T("%s"), path); //得到文件路径
		delete buff;
		//设置FLASH起始地址   修改成BIN文件的起始地址通过外部设置成员值
		//	m_Upflashaddr = DefFlashAddr;
	}
	else
	{
		AfxMessageBox(_T("打开文件失败"));
		return false;
	}
	return true;
}


//导入HEX升级文件
bool Cusart_isp::Install_Hex_Upfile_path(LPCTSTR path)
{
	CStdioFile Hexfile;  //文件对象
	CString datastring;  //文件一行的数据
	DWORD fsize = 0;    //文件大小

	if (Hexfile.Open(path, CStdioFile::modeRead) == true)  //打开文件
	{
		fsize = Hexfile.GetLength(); //文件大小
		//起始文件地址标记 最高位为乒乓模式 表示设置地址 bit0表示是否为第一次设置地址 bit1表示已经设置过地址
		//bit3 退出标记
		TBYTE  firstaddr = 0;

		DWORD flashaddr = 0; //HEX文件中指出的地址
		DWORD flashaddr_last = 0; //上一次的地址

		TBYTE *flashdata = new TBYTE[fsize];  //内存申请 解析后的数据
		DWORD offset = 0;  //偏移

		TBYTE databuff[1024];  //文件一行的数据  解析时用到
		int error = 0;  //错误参数
		//读取一行数据出错   错误标记   退出标记   
		while ((Hexfile.ReadString((CString &)datastring) == true) && (error == 0) && ((firstaddr & 0x08) == 0x00))
		{
			//检测文件数据是否正常  包括标记、校验
			error = Test_HexOneData(datastring, databuff, sizeof(databuff));
			if (error == 0) //正常
			{
				//如果还没有设置过地址  则第一条记录必须为设置线性地址标记
				//上一条记录为设置线性地址标记 则下一条记录必须为数据记录
				if ((((firstaddr & 0x03) == 0) && (databuff[3] != 0X04)) || ((firstaddr & 0x80) && (databuff[3] != 0X00)))
				{
					error = 0x56;  //标记错误
					break;  //退出
				}

				switch (databuff[3])  //记录类型
				{
				case 0X00: //数据记录
					//得到FLASH中的地址 低两个字节
					flashaddr = (flashaddr & 0xffff0000) | (((WORD)databuff[1] << 8) | databuff[2]);
					if ((firstaddr & 0x03) == 0x01)  //如果是第一次设置地址  则直接赋值
					{
						firstaddr |= 0x03; //标记已经设置过地址  则接下来的地址必须为连贯的
						flashaddr_last = flashaddr;  //直接设置地址
						m_Upflashaddr = flashaddr_last; //得到FLASH 的起始地址
					}
					if (flashaddr != flashaddr_last)  //如果数据地址不等
					{
						error = 0x55;  //标记错误
						continue; //退出
					}
					//把数据添加到数据缓存
					for (size_t i = 0; i < databuff[0]; i++)
					{
						flashdata[offset] = databuff[4 + i];
						offset++;
					}
					flashaddr_last += databuff[0]; //地址偏移
					firstaddr &= ~0x80; //清楚乒乓标记
					break;
				case 0X01: //文件结束记录 
					firstaddr |= 0x08; //标记文件结束
					continue;
					break;
				case 0X02: //扩展段地址记录
					firstaddr |= 0x40;  //测试
					break;
				case 0X03: //起始段地址类型记录
					firstaddr |= 0x40;  //测试
					break;
				case 0X04: //扩展线性地址
					if ((databuff[1] != 0) || (databuff[2] != 0))
					{
						error = 0x57;  //标记错误
						continue; //退出
					}
					for (size_t i = 0; i < databuff[0]; i++)
					{
						flashaddr_last <<= 8;
						flashaddr_last |= databuff[4 + i];
					}
					flashaddr_last <<= 16;  //左移16  此记录为高两字节
					flashaddr = flashaddr_last;  //备份地址
					firstaddr |= 0X80;  //标记为接收到扩展线性地址   
					if ((firstaddr & 0X03) == 0) //如果是第一次设置地址  则表示为FLASH的起始地址
					{
						firstaddr |= 0X01;  //标记接收到
					}
					break;
				case 0X05://起始线性地址类型记录
					firstaddr |= 0x40;  //测试
					break;
				default:
					delete flashdata; //释放内存
					CString errstr;
					errstr.Format(_T("未知记录类型: 0X%02X"), databuff[3]);
					::AfxMessageBox(errstr);
					return false;
				}
			}
		}
		if (error > 0)  //文件格式错误
		{
			Hexfile.Close();
			delete flashdata; //释放内存
			switch (error)
			{
			case 1:
				::AfxMessageBox(_T("接收缓存太小"));
				break;
			case 2:
				::AfxMessageBox(_T("HEX文件格式错误 起始标记或者长度错误"));
				break;
			case 0X55:
				::AfxMessageBox(_T("HEX文件格式错误 FLASH地址不连贯"));
				break;
			case 0X56:
				::AfxMessageBox(_T("HEX文件格式错误 记录流程错误"));
				break;
			case 0X57:
				::AfxMessageBox(_T("HEX文件格式错误 扩展线性地址不为0000"));
				break;
			default:
				::AfxMessageBox(_T("HEX文件格式错误"));
				break;
			}
			return false;
		}
		Hexfile.Close();   //关闭文件
		Upfdata.RemoveAll(); //清除所有
		Upfdata.SetSize(offset); //设置大小
		for (size_t i = 0; i < offset; i++)
		{
			Upfdata[i] = flashdata[i]; //拷贝文件数据
		}
		delete flashdata; //释放内存
		Upfpath = path;  //得到文件路径
		Upfsize = Upfdata.GetCount(); //文件数据大小
		return true;
	}
	::AfxMessageBox(_T("打开HEX文件失败"));
	return false;
}






/*
*发送数据并接收数据
*send 需要发送的数据 sendlen 发送的数据长度
*rec 接收数据缓存 reclen接收的数据长度 如果为负数 表示接收长度任意 绝对值为接收缓存大小
*Maxtime 超时时间 单位 MS
*/
bool Cusart_isp::Send_data_Rec(TBYTE *send, int sendlen, TBYTE *rec, int reclen, DWORD Maxtime)
{
	if (IsPortComOpen())
	{
		m_recbufCount = 0;  //接收缓存偏移地址清零
		CString debstr;
		debstr.Format(_T("发送%d个字节:\r\n    "), sendlen);
		for (size_t i = 0; i < sendlen; i++)
		{
			CString string;
			string.Format(_T(" 0X%02X"), send[i]);
			debstr += string;
		}
		debstr += _T("\r\n");
		OutputDebugString(debstr);
		debstr.Format(_T("")); //清除
		if (WriteData(send, sendlen))
		{
			DWORD Count = Maxtime;
			WORD offset = 0; //当前接收到的数据长度 偏移
			int lensize = abs(reclen); //得到绝对值
			int Minlensize = 0; //最小值
			do
			{
				Sleep(1);
				offset = m_recbufCount;
				if ((offset == lensize) || ((offset > 0) && (reclen < 0)))
				{
					Minlensize = min(m_recbufCount, lensize); //得到最小值
					memcpy(rec, m_recbuf, Minlensize);  //把此次接收到的数据拷贝
					debstr.Format(_T("接收%d个字节:\r\n    "), offset);
					for (size_t i = 0; i < offset; i++)
					{
						CString string;
						string.Format(_T(" 0X%02X"), rec[i]);
						debstr += string;
					}
					debstr += _T("\r\n");
					OutputDebugString(debstr);
					return true;
				}
			} while (--Count);
		}
	}
	return false;
}

bool Cusart_isp::Get_command(void)
{
	TBYTE Sendbuf[512] = { 0X00, 0XFF };
	TBYTE Recbuf[512];
	OutputDebugString(_T("发送Get Command命令\r\n"));
	if (Send_data_Rec(Sendbuf, 2, Recbuf, 15))
	{
		if ((Recbuf[0] == Recbuf[14]) && (Recbuf[14] == MCU_ACK) && (Recbuf[1] == (15 - 3 - 1)))
		{
			m_Bootloader_Ver = Recbuf[2]; //得到BOOTLOADER 版本
			CString string;
			string.Format(_T("Bootloader 版本 %d.%d\r\n"), m_Bootloader_Ver / 16, m_Bootloader_Ver % 16);
			OutputDebugString(string);
			return true;
		}
	}
	return false;
}

bool Cusart_isp::Get_Version_command(void)
{
	TBYTE Sendbuf[512] = { 0X01, 0XFE };
	TBYTE Recbuf[512];
	OutputDebugString(_T("发送Get Version Command命令\r\n"));
	if (Send_data_Rec(Sendbuf, 2, Recbuf, 5))
	{
		if ((Recbuf[0] == Recbuf[4]) && (Recbuf[4] == MCU_ACK))
		{
			m_Bootloader_Ver = Recbuf[1]; //得到BOOTLOADER 版本
			CString string;
			string.Format(_T("Bootloader 版本 %d.%d\r\n"), m_Bootloader_Ver >> 4, m_Bootloader_Ver & 0x0f);
			OutputDebugString(string);
			return true;
		}
	}
	return false;
}

bool Cusart_isp::Get_ID_command(void)
{
	TBYTE Sendbuf[512] = { 0X02, 0XFD };
	TBYTE Recbuf[512];
	int Recsize = 5;  //接收的数据长度
	OutputDebugString(_T("发送Get ID Command命令"));
	if (Send_data_Rec(Sendbuf, 2, Recbuf, Recsize))
	{
		if ((Recbuf[0] == Recbuf[4]) && (Recbuf[4] == MCU_ACK) && (Recbuf[1] = (Recsize - 3 - 1)))
		{
			//得到PID
			m_Mcu_PID = Recbuf[2];
			m_Mcu_PID = (m_Mcu_PID << 8) | Recbuf[3];
			CString string;
			string.Format(_T("PID: 0X%08X\r\n"), m_Mcu_PID);
			OutputDebugString(string);
			return true;
		}
	}
	return false;
}

bool Cusart_isp::Read_Memory_command(DWORD addr, TBYTE * BUFF, WORD len)
{
	TBYTE Sendbuf[512] = { 0X11, 0XEE };
	TBYTE Recbuf[512];
	int Recsize = 1;  //接收的数据长度
	OutputDebugString(_T("发送Read Memory Command命令"));
	if (Send_data_Rec(Sendbuf, 2, Recbuf, Recsize))  //发送命令
	{
		if (Recbuf[0] == MCU_ACK)
		{
			Sendbuf[0] = (addr & 0xff000000) >> 24;
			Sendbuf[1] = (addr & 0x00ff0000) >> 16;
			Sendbuf[2] = (addr & 0x0000ff00) >> 8;
			Sendbuf[3] = (addr & 0x000000ff);
			Sendbuf[4] = Return_Xor(Sendbuf, 4);  //得到异或校验值
			if (Send_data_Rec(Sendbuf, 5, Recbuf, Recsize)) //发送地址
			{
				if (Recbuf[0] == MCU_ACK)
				{
					Sendbuf[0] = len - 1; //发送的数据为实际接收数据长度 -1
					Sendbuf[1] = Sendbuf[0] ^ 0xff;
					Recsize = len + 1; //需要接收数据长度  第一个字节为ACK后面紧接着为数据
					if (Send_data_Rec(Sendbuf, 2, Recbuf, Recsize)) //发送读数据数量
					{
						if (Recbuf[0] == MCU_ACK)
						{
							memcpy(BUFF, &Recbuf[1], len);
							return true;
						}
					}
				}
			}
		}
	}
	return false;
}
//写指定地址数据  长度最大为256   长度必须为4的倍数
bool Cusart_isp::Write_Memory_command(DWORD addr, TBYTE * BUFF, WORD len)
{
	TBYTE Sendbuf[512] = { 0X31, 0XCE };
	TBYTE Recbuf[512];
	int Recsize = 1;  //接收的数据长度
	OutputDebugString(_T("发送Write Memory Command命令"));
	if (Send_data_Rec(Sendbuf, 2, Recbuf, Recsize))  //发送命令
	{
		if (Recbuf[0] == MCU_ACK)
		{
			Sendbuf[0] = (addr & 0xff000000) >> 24;
			Sendbuf[1] = (addr & 0x00ff0000) >> 16;
			Sendbuf[2] = (addr & 0x0000ff00) >> 8;
			Sendbuf[3] = (addr & 0x000000ff);
			Sendbuf[4] = Return_Xor(Sendbuf, 4);  //得到异或校验值
			if (Send_data_Rec(Sendbuf, 5, Recbuf, Recsize)) //发送地址
			{
				if (Recbuf[0] == MCU_ACK)
				{
					Sendbuf[0] = len - 1; //发送的数据为实际需要些的数据长度 -1
					memcpy(&Sendbuf[1], BUFF, len);//拷贝需要发送的数据
					Sendbuf[len + 1] = Return_Xor(Sendbuf, len + 1);  //得到异或校验值
					if (Send_data_Rec(Sendbuf, len + 2, Recbuf, Recsize)) //发送读数据数量
					{
						if (Recbuf[0] == MCU_ACK)
						{
							return true;
						}
					}
				}
			}
		}
	}
	return false;
}

//异或校验
TBYTE Cusart_isp::Return_Xor(TBYTE *Data, int len)
{
	TBYTE Xor = 0;
	if (len == 1)
	{
		Xor = Data[0] ^ 0xff;
		return  Xor;
	}
	for (size_t i = 0; i < len; i++)
	{
		Xor ^= Data[i];
	}
	return Xor;
}

//返回字符数组累加值
DWORD Cusart_isp::Return_Add(TBYTE *Data, int len)
{
	DWORD  add_data = 0;
	for (size_t i = 0; i < len; i++)
	{
		add_data += Data[i];
	}
	return add_data;
}


//检测一个字符串 是否为HEX文件格式
//Hexbuf 解析后的十六进制数据  len 缓存Hexbuf的长度
//返回0正常 1缓存长度不足   2不为HEX数据格式
int Cusart_isp::Test_HexOneData(CString string, TBYTE *Hexbuf, int len)
{
	CString dataone(string); //得到一行数据
	//第一个字符必须为： 且长度必须大于等于11 且长度减一后必须为偶数
	if ((dataone[0] != ':') || (dataone.GetLength() < 11) || ((dataone.GetLength() - 1) % 2))
	{
		return 2;
	}
	WORD Count = 0;  //字符串的长度
	Count = dataone.GetLength(); //得到长度
	for (size_t i = 1; i < Count; i++)  //检查
	{
		if (::isxdigit(string[i]) == false)  //检查是否为十六进制数
		{
			return 3;  //不为HEX格式
		}
	}
	//到达这里 表示字符串为正确的HEX格式且都为十六进制字符
	TBYTE * databuff = new TBYTE[(Count - 1) / 2]; //申请缓存
	for (size_t i = 0; i < (Count - 1) / 2; i++)
	{
		CString tempstr("");
		tempstr += string.Mid(1 + 2 * i, 2);
		//把ASC转换成十六进制
		databuff[i] = (TBYTE)strtol((const char *)tempstr.GetBuffer(), NULL, 16);
	}
	Count = (Count - 1) / 2;  //得到十六进制数据长度
	//检查长度
	if ((databuff[0] + 5) != Count)
	{
		delete databuff;//释放内存
		return 4;  //不为HEX格式   长度错误
	}
	//检查校验值
	TBYTE byte = 0;
	byte = (TBYTE)Return_Add(databuff, Count - 1);
	byte = (byte ^ 0xff) + 1;  //取反后加1 为校验值
	if (byte != databuff[Count - 1])
	{
		delete databuff;//释放内存
		return 5;  //不为HEX格式   校验值不对
	}
	//到此 此行数据已经全部检测完成  为HEX文件格式 开始检查接收数据缓存
	if (len < Count)
	{
		delete databuff;//释放内存
		return 1;  //接收缓存太小
	}
	//拷贝装好好的数据到数据缓存
	memcpy(Hexbuf, databuff, Count);
	delete databuff;//释放内存
	return 0; //成功
}



bool Cusart_isp::Go_command(DWORD addr)
{
	TBYTE Sendbuf[512] = { 0X21, 0XDE };
	TBYTE Recbuf[512];
	int Recsize = 1;  //接收的数据长度
	OutputDebugString(_T("发送Go Command命令"));
	if (Send_data_Rec(Sendbuf, 2, Recbuf, Recsize))  //发送命令
	{
		if (Recbuf[0] == MCU_ACK)
		{
			Sendbuf[0] = (addr & 0xff000000) >> 24;
			Sendbuf[1] = (addr & 0x00ff0000) >> 16;
			Sendbuf[2] = (addr & 0x0000ff00) >> 8;
			Sendbuf[3] = (addr & 0x000000ff);
			Sendbuf[4] = Return_Xor(Sendbuf, 4);  //得到异或校验值
			if (Send_data_Rec(Sendbuf, 5, Recbuf, Recsize)) //发送地址
			{
				if (Recbuf[0] == MCU_ACK)
				{
					return true;
				}
			}
		}
	}
	return false;
}

//擦除命令Secbuf 需要擦除的扇区号缓存  number 擦除的扇区数 当为0XFF时 则擦除全部扇区
bool Cusart_isp::Erase_Memory_command(TBYTE * Secbuf, TBYTE number)
{
	TBYTE Sendbuf[512] = { 0X43, 0XBC };
	TBYTE Recbuf[512];
	int Recsize = 1;  //接收的数据长度
	int Sendsize = 0; //需要发送的数据长度
	OutputDebugString(_T("发送Erase Memory Command命令"));
	if (Send_data_Rec(Sendbuf, 2, Recbuf, Recsize))  //发送命令
	{
		if (Recbuf[0] == MCU_ACK)
		{
			if (number == 0XFF) //全部擦除
			{
				Sendbuf[0] = 0XFF;
				Sendbuf[1] = 0X00;
				Sendsize = 2; //需要发送的数据长度
			}
			else
			{
				Sendbuf[0] = number - 1;//需要擦除的扇区数
				memcpy(&Sendbuf[1], Secbuf, number); //扇区编号
				Sendbuf[number + 1] = Return_Xor(Sendbuf, number + 1); //校验值
				Sendsize = number + 2; //需要发送的数据长度
			}
			if (Send_data_Rec(Sendbuf, Sendsize, Recbuf, Recsize, 60 * 1000))  //发送命令
			{
				if (Recbuf[0] == MCU_ACK)
				{
					return true;
				}
			}
		}
	}
	return false;
}
//延伸擦除命令  擦除所有扇区
bool Cusart_isp::Extended_Erase_Memory_command(void)
{
	TBYTE Sendbuf[512] = { 0X44, 0XBB };
	TBYTE Recbuf[512];
	int Recsize = 1;  //接收的数据长度
	int Sendsize = 2; //需要发送的数据长度
	OutputDebugString(_T("发送Erase Memory Command命令"));
	if (Send_data_Rec(Sendbuf, Sendsize, Recbuf, Recsize))  //发送命令
	{
		if (Recbuf[0] == MCU_ACK)
		{
			Sendbuf[0] = 0XFF;  //擦除所有扇区
			Sendbuf[1] = 0XFF;
			Sendbuf[2] = 0X00;
			Sendbuf[3] = 0X00;
			Sendsize = 4; //需要发送的数据长度
			if (Send_data_Rec(Sendbuf, Sendsize, Recbuf, Recsize, 60 * 1000))  //发送命令
			{
				if (Recbuf[0] == MCU_ACK)
				{
					return true;
				}
			}
		}
	}
	return false;
}



//写保护  Secbuf 扇区号缓存  number 扇区数
bool Cusart_isp::Write_Protect_command(TBYTE * Secbuf, TBYTE number)
{
	TBYTE Sendbuf[512] = { 0X63, 0X9C };
	TBYTE Recbuf[512];
	int Recsize = 1;  //接收的数据长度
	int Sendsize = 0; //需要发送的数据长度
	OutputDebugString(_T("发送Write Protect Command命令"));
	if (Send_data_Rec(Sendbuf, 2, Recbuf, Recsize))  //发送命令
	{
		if (Recbuf[0] == MCU_ACK)
		{
			Sendbuf[0] = number - 1;//需要擦除的扇区数
			memcpy(&Sendbuf[1], Secbuf, number); //扇区编号
			Sendbuf[number + 1] = Return_Xor(Sendbuf, number + 1); //校验值
			Sendsize = number + 2; //需要发送的数据长度
			if (Send_data_Rec(Sendbuf, Sendsize, Recbuf, Recsize))  //发送命令
			{
				if (Recbuf[0] == MCU_ACK)
				{
					return true;
				}
			}
		}
	}
	return false;
}

//去除写保护
bool Cusart_isp::Write_Unprotect_command(void)
{
	TBYTE Sendbuf[512] = { 0X73, 0X8C };
	TBYTE Recbuf[512];
	int Recsize = 2;  //接收的数据长度
	int Sendsize = 2; //需要发送的数据长度
	OutputDebugString(_T("发送Write Unprotect Command命令"));
	if (Send_data_Rec(Sendbuf, Sendsize, Recbuf, Recsize))  //发送命令
	{
		if ((Recbuf[0] == MCU_ACK) && (Recbuf[1] == MCU_ACK))
		{
			return true;
		}
	}
	return false;
}


//读保护
bool Cusart_isp::Readout_Protect_command(void)
{
	TBYTE Sendbuf[512] = { 0X82, 0X7D };
	TBYTE Recbuf[512];
	int Recsize = 2;  //接收的数据长度
	int Sendsize = 2; //需要发送的数据长度
	OutputDebugString(_T("发送Readout Protect Command命令"));
	if (Send_data_Rec(Sendbuf, Sendsize, Recbuf, Recsize))  //发送命令
	{

		if ((Recbuf[0] == MCU_ACK) && (Recbuf[1] == MCU_ACK))
		{
			return true;
		}

	}
	return false;
}

//去除读保护
bool Cusart_isp::Readout_Unprotect_command(void)
{
	TBYTE Sendbuf[512] = { 0X92, 0X6D };
	TBYTE Recbuf[512];
	int Recsize = 2;  //接收的数据长度
	int Sendsize = 2; //需要发送的数据长度
	OutputDebugString(_T("发送Readout Unprotect Command命令"));
	if (Send_data_Rec(Sendbuf, Sendsize, Recbuf, Recsize))  //发送命令
	{
		if ((Recbuf[0] == MCU_ACK) && (Recbuf[1] == MCU_ACK))
		{
			return true;
		}
	}
	return false;
}

//设置MCU进入ISP模式
bool Cusart_isp::Set_Mcu_to_Isp(void)
{
	TBYTE Sendbuf[512] = { 0X7F, 0X7F, 0X7F, 0X7F, 0X7F };
	TBYTE Recbuf[512];
	int Recsize = -3;  //接收的数据长度
	int Sendsize = 5; //需要发送的数据长度
	OutputDebugString(_T("发送波特率自动同步字符0X7F \r\n"));
	if (Send_data_Rec(Sendbuf, Sendsize, Recbuf, Recsize))  //发送命令
	{
		if (Recbuf[0] == MCU_ACK)
		{
			return true;
		}
	}
	return false;
}

/*
* 执行升级文件  升级流程
*/
bool Cusart_isp::Updata_file_exe(void)
{

	if (Set_Mcu_IO_to_Isp() == false)  //配置MCU boot0为高电平 进入ISP
	{
		::AfxMessageBox(_T("配置RTS及DTR引脚失败 请检查串口"));
		return false;
	}
	if (Set_Mcu_to_Isp() == false)  //进入ISP模式
	{
		::AfxMessageBox(_T("进入ISP模式错误"));
		return false;
	}
	if (Get_Version_command() == false)  //返回版本
	{
		::AfxMessageBox(_T("Get_Version_command 错误"));
		return false;
	}

	if (Get_ID_command() == false)  //返回PID信息
	{
		::AfxMessageBox(_T("Get_ID_command 错误"));
		return false;
	}
	if (Erase_Memory_command() == false)  //擦除全部扇区
	{
		if (Extended_Erase_Memory_command() == false)
		{
			::AfxMessageBox(_T("Extended_Erase_Memory_command 错误"));
			return false;
		}
		if (Set_Mcu_IO_to_Isp() == false)  //配置MCU boot0为高电平 进入ISP
		{
			::AfxMessageBox(_T("配置RTS及DTR引脚失败 请检查串口"));
			return false;
		}
		if (Set_Mcu_to_Isp() == false)  //进入ISP模式
		{
			::AfxMessageBox(_T("进入ISP模式错误"));
			return false;
		}
		if (Get_Version_command() == false)  //返回版本
		{
			::AfxMessageBox(_T("Get_Version_command 错误"));
			return false;
		}
	}
	//开始下载升级文件
	TBYTE Sendbuff[1024]; //发送缓存
	TBYTE Recbuff[1024]; //接收缓存
	int SendSize = 0; //当前发送的数据长度

	Upwaddr = m_Upflashaddr; //当前写地址
	Upfsize = Upfdata.GetCount(); //得到文件大小
	Upfsizeoff = 0; //文件地址偏移
	TBYTE *filedata = new TBYTE[Upfsize]; //申请内存
	for (size_t i = 0; i < Upfsize; i++)
	{
		filedata[i] = Upfdata[i]; //拷贝文件数据
	}

	while (Upfsize > Upfsizeoff)
	{
		//当前写数据缓存大小
		SendSize = ((Upfsize - Upfsizeoff) >= IspDataSecSize) ? IspDataSecSize : (Upfsize - Upfsizeoff);
		memcpy(Sendbuff, &filedata[Upfsizeoff], SendSize); //拷贝数据
		//写入数据
		if (Write_Memory_command(Upwaddr, Sendbuff, SendSize) != true)
		{
			delete filedata;
			::AfxMessageBox(_T("Write_Memory_command 错误"));
			return false;
		}
		//读出刚才写入的数据
		if (Read_Memory_command(Upwaddr, Recbuff, SendSize) != true)
		{
			delete filedata;
			::AfxMessageBox(_T("Read_Memory_command 错误"));
			return false;
		}
		//比较写入与读出的数据是否相同
		if (memcmp(Sendbuff, Recbuff, SendSize) != 0)
		{
			delete filedata;
			::AfxMessageBox(_T("写入与读出不一致"));
			return false;
		}
		Upwaddr += SendSize;//写地址偏移
		Upfsizeoff += SendSize; //文件读写指针偏移
		Set_Prog_Val(Upfsizeoff, Upfsize); //设置进度条
	}
	delete filedata;  //释放缓存
	if (Go_command(m_Upflashaddr) == false)  //跳转
	{
		::AfxMessageBox(_T("Go_command命令错误"));
		return false;
	}
	return true;
}

//设置进度条
void Cusart_isp::Set_Prog_Val(DWORD Val, DWORD max)
{
	DWORD now_val = Val * 100 / max;
	if (m_Prog != NULL)
	{
		m_Prog->SetPos(now_val);
	}
	if (m_Progtxt != NULL)
	{
		now_val = Val * 1000 / max;
		CString play;
		play.Format(_T("%02d.%01d%%"), now_val / 10, now_val % 10);
		m_Progtxt->SetWindowText(play);
	}
}


//配置串口引脚的RTS及 DTR 引脚  使MCU进入ISP状态
//RTS与RTS# 是反逻辑    DTR与DTR# 是反逻辑   即控制RTS引脚输出高电平则RTS#引脚输出低电平
bool Cusart_isp::Set_Mcu_IO_to_Isp(void)
{
	OutputDebugString(_T("\r\n配置串口 RTS及DTR引脚 使MCU进入ISP \r\n"));
	//先配置RTS#输出低电平   使BOOT0引脚为高电平
	if (Set_EscapeCommFunction(SETRTS) == false)
	{
		return false;
	}
	Sleep(100);
	//配置DTR#输出高电平   MCU复位
	if (Set_EscapeCommFunction(CLRDTR) == false)
	{
		return false;
	}
	Sleep(100);
	//配置DTR#输出低电平  释放MCU复位
	if (Set_EscapeCommFunction(SETDTR) == false)
	{
		return false;
	}
	Sleep(100);

	//配置RTS#输出高电平   使MCU BOOT0引脚为低电平
	if (Set_EscapeCommFunction(CLRRTS) == false)
	{
		return false;
	}
	return true;
}


//当输入缓存为空时 返回Bin文件数据大小
//当输入缓存不为空时 则填充数据缓存
int Cusart_isp::GetFileDataCount(TBYTE *Data)
{
	if (Data == NULL)
	{
		return Upfdata.GetCount();
	}

	WORD count = Upfdata.GetCount();
	if (count == 0)
	{
		return -1;
	}
	for (WORD i = 0; i < count; i++)
	{
		Data[i] = Upfdata[i];
	}
	return 1;
}