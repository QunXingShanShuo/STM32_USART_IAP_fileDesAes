#include "stdafx.h"
#include "MXXX_File.h"
#include "string.h"
//#include "DES_DLL.H"
//#include "AES_DLL.H"
#include "DES.H"
#include "AES.H"


CMXXX_File::CMXXX_File()
{
	m_EnDeFileData = NULL;
	m_FileData = NULL;
}


CMXXX_File::~CMXXX_File()
{
	if (m_EnDeFileData != NULL)
	{
		delete m_EnDeFileData;
	}
	if (m_FileData != NULL)
	{
		delete m_FileData;
	}
}



//装载文件
bool CMXXX_File::Install_File_Path(CString filepath)
{
	if (::PathFileExists(filepath) == false) //检查文件或者文件夹是否存在
	{
		::AfxMessageBox(_T("文件不存在"));
		return false;
	}
	if (m_FileData != NULL)
	{
		delete m_FileData;
		m_FileData = NULL;
	}
	CFile file;
	if (file.Open(filepath, CFile::modeRead) == TRUE)  //打开文件
	{
		m_FileSize = file.GetLength();  //得到文件大小
		m_FileData = new TBYTE[m_FileSize];  //申请内存
		file.Read(m_FileData, m_FileSize);    //读取所有数据
		file.Close();
	}
	else
	{
		::AfxMessageBox(_T("打开文件失败"));
		return false;
	}
	return TRUE;
}


bool CMXXX_File::Des_Encrypt_CBCString(TBYTE* IV, TBYTE *mes, TBYTE *EnData, TBYTE* Key, WORD KeySize, DWORD64 mesSize)
{
	void(*DES_En_Fun)(TBYTE*, TBYTE*, TBYTE*);  //DES加密执行函数 指针
	switch (KeySize)
	{
	case 8:
		DES_En_Fun = One_DES_IV_Encrypt_Block;
		break;
	case 16:
		DES_En_Fun = Two_DES_IV_Encrypt_Block;
		break;
	case 24:
		DES_En_Fun = Three_DES_IV_Encrypt_Block;
		break;
	default:
		TRACE(_T("\r\nDES加密密钥大小错误\r\n"));
		::AfxMessageBox(_T("DES加密密钥大小错误"));
		return false;
		break;
	}
	if (mesSize % 8)
	{
		CString str;
		str.Format(_T("\r\nDES加密明文数据长度错误  长度：%ld"), mesSize);
		TRACE(_T("\r\n") + str + _T("\r\n"), mesSize);
		::AfxMessageBox(str);
		return false;
	}
	DWORD64 Count = mesSize / 8;  //加密次数
	DWORD64 Count_i = 0;
	TBYTE IvData[8];  //向量缓存
	memcpy(IvData, IV, 8);  //拷贝向量
	TBYTE *EncryBuf = new TBYTE[mesSize];  //申请内存
	memcpy(EncryBuf, mes, mesSize);  //拷贝明文
	for (Count_i = 0; Count_i < Count; Count_i++)
	{
		DES_En_Fun(IvData, &EncryBuf[Count_i * 8], Key);
	}
	memcpy(EnData, EncryBuf, mesSize);  //拷贝密文
	delete EncryBuf;  //释放内存
	return true;
}




bool CMXXX_File::Aes_Encrypt_CBCString(TBYTE* IV, TBYTE *mes, TBYTE *EnData, TBYTE* Key, WORD KeySize, DWORD64 mesSize)
{
	void(*DES_En_Fun)(TBYTE*, TBYTE*, TBYTE*);  //DES加密执行函数 指针
	switch (KeySize)
	{
	case 16:
		DES_En_Fun = Aes_IV_key128bit_Encrypt;
		break;
	case 24:
		DES_En_Fun = Aes_IV_key192bit_Encrypt;
		break;
	case 32:
		DES_En_Fun = Aes_IV_key256bit_Encrypt;
		break;
	default:
		TRACE(_T("\r\nAES加密密钥大小错误\r\n"));
		::AfxMessageBox(_T("AES加密密钥大小错误"));
		return false;
		break;
	}
	if (mesSize % 16)
	{
		CString str;
		str.Format(_T("\r\nAES加密明文数据长度错误  长度：%ld"), mesSize);
		TRACE(_T("\r\n") + str + _T("\r\n"), mesSize);
		::AfxMessageBox(str);
		return false;
	}
	DWORD64 Count = mesSize / 16;  //加密次数
	DWORD64 Count_i = 0;
	TBYTE IvData[16];  //向量缓存
	memcpy(IvData, IV, 16);  //拷贝向量
	TBYTE *EncryBuf = new TBYTE[mesSize];  //申请内存
	memcpy(EncryBuf, mes, mesSize);  //拷贝明文
	for (Count_i = 0; Count_i < Count; Count_i++)
	{
		DES_En_Fun(IvData, &EncryBuf[Count_i * 16], Key);
	}
	memcpy(EnData, EncryBuf, mesSize);  //拷贝密文
	delete EncryBuf;  //释放内存
	return true;
}



//DES解密 CBC  KeySize 只能为 8 16  24   
bool CMXXX_File::Des_Decode_CBCString(TBYTE* IV, TBYTE *mes, TBYTE *EnData, TBYTE* Key, WORD KeySize, DWORD64 mesSize)
{
	void(*DES_En_Fun)(TBYTE*, TBYTE*, TBYTE*);  //DES加密执行函数 指针
	switch (KeySize)
	{
	case 8:
		DES_En_Fun = One_DES_IV_Decode_Block;
		break;
	case 16:
		DES_En_Fun = Two_DES_IV_Decode_Block;
		break;
	case 24:
		DES_En_Fun = Three_DES_IV_Decode_Block;
		break;
	default:
		TRACE(_T("\r\nDES解密密钥大小错误\r\n"));
		::AfxMessageBox(_T("DES解密密钥大小错误"));
		return false;
		break;
	}
	if (mesSize % 8)
	{
		CString str;
		str.Format(_T("\r\nDES解密密文数据长度错误  长度：%ld"), mesSize);
		TRACE(_T("\r\n") + str + _T("\r\n"), mesSize);
		::AfxMessageBox(str);
		return false;
	}
	DWORD64 Count = mesSize / 8;  //加密次数
	DWORD64 Count_i = 0;
	TBYTE IvData[16];  //向量缓存
	memcpy(IvData, IV, 16);  //拷贝向量
	TBYTE *EncryBuf = new TBYTE[mesSize];  //申请内存
	memcpy(EncryBuf, mes, mesSize);  //拷贝明文
	for (Count_i = 0; Count_i < Count; Count_i++)
	{
		DES_En_Fun(IvData, &EncryBuf[Count_i * 8], Key);
	}
	memcpy(EnData, EncryBuf, mesSize);  //拷贝密文
	delete EncryBuf;  //释放内存
	return true;
}

//AES解密 CBC  KeySize 只能为 16 24  32   decode
bool CMXXX_File::Aes_Decode_CBCString(TBYTE* IV, TBYTE *mes, TBYTE *EnData, TBYTE* Key, WORD KeySize, DWORD64 mesSize)
{
	void(*DES_En_Fun)(TBYTE*, TBYTE*, TBYTE*);  //DES加密执行函数 指针
	switch (KeySize)
	{
	case 16:
		DES_En_Fun = Aes_IV_key128bit_Decode;
		break;
	case 24:
		DES_En_Fun = Aes_IV_key192bit_Decode;
		break;
	case 32:
		DES_En_Fun = Aes_IV_key256bit_Decode;
		break;
	default:
		TRACE(_T("\r\nAES加密密钥大小错误\r\n"));
		::AfxMessageBox(_T("AES加密密钥大小错误"));
		return false;
		break;
	}
	if (mesSize % 16)
	{
		CString str;
		str.Format(_T("\r\nAES解密密文数据长度错误  长度：%ld"), mesSize);
		TRACE(_T("\r\n") + str + _T("\r\n"), mesSize);
		::AfxMessageBox(str);
		return false;
	}
	DWORD64 Count = mesSize / 16;  //加密次数
	DWORD64 Count_i = 0;
	TBYTE IvData[16];  //向量缓存
	memcpy(IvData, IV, 16);  //拷贝向量
	TBYTE *EncryBuf = new TBYTE[mesSize];  //申请内存
	memcpy(EncryBuf, mes, mesSize);  //拷贝明文
	for (Count_i = 0; Count_i < Count; Count_i++)
	{
		DES_En_Fun(IvData, &EncryBuf[Count_i * 16], Key);
	}
	memcpy(EnData, EncryBuf, mesSize);  //拷贝密文
	delete EncryBuf;  //释放内存
	return true;
}


//执行文件加密
bool CMXXX_File::File_Encrypt_exe(CString Userstring, TBYTE *Ivdata, TBYTE * Keydata, WORD KeySize, bool DesFlat)
{
	if (m_FileData == NULL)
	{
		::AfxMessageBox(_T("未载入文件数据"));
		return  FALSE;
	}
	DWORD32 strlen = Userstring.GetLength();
	m_EnDeFileSize = 4 + strlen + 4;  //文件头长度
	m_EnDeFileSize += (m_FileSize % 16) ? ((m_FileSize / 16 + 1) * 16) : m_FileSize;  //文件数据大小 这里要转换为16的整数倍
	if (m_EnDeFileData != NULL)
	{
		delete m_EnDeFileData;
		m_EnDeFileData = NULL;
	}
	m_EnDeFileData = new TBYTE[m_EnDeFileSize];  //申请内存
	memcpy(m_EnDeFileData, &strlen, 4);  //拷贝自定义数据长度
	memcpy(&m_EnDeFileData[4], Userstring.GetBuffer(), strlen);  //拷贝自定义数据
	memcpy(&m_EnDeFileData[4 + strlen], &m_FileSize, 4);  //拷贝文件数据长度
	memcpy(&m_EnDeFileData[4 + strlen + 4], m_FileData, m_FileSize); //拷贝文件数据
	delete m_FileData;  //释放内存
	m_FileData = NULL;
	if (DesFlat)  //如果是DES加密
	{
		if (Des_Encrypt_CBCString(Ivdata, m_EnDeFileData, m_EnDeFileData, Keydata, KeySize, m_EnDeFileSize) == false)
		{
			return  false;
		}
	}
	else  //AES 加密
	{
		if (Aes_Encrypt_CBCString(Ivdata, m_EnDeFileData, m_EnDeFileData, Keydata, KeySize, m_EnDeFileSize) == false)
		{
			return  false;
		}
	}
	return true;
}



bool CMXXX_File::Save_EncryptFile(CString filepath)
{
	if (m_EnDeFileData == NULL)
	{
		::AfxMessageBox(_T("没有加密数据"));
		return  FALSE;
	}
	CFile file;
	if (file.Open(filepath, CFile::modeCreate | CFile::modeReadWrite) == true)
	{
		file.Write(m_EnDeFileData, m_EnDeFileSize);
		file.Close();
		return true;
	}
	else
	{
		::AfxMessageBox(_T("文件创建失败"));
	}
	return  FALSE;
}

bool CMXXX_File::Save_DecodeFile(CString filepath)
{
	if (m_EnDeFileData == NULL)
	{
		::AfxMessageBox(_T("没有解密数据"));
		return  FALSE;
	}
	CFile file;
	if (file.Open(filepath, CFile::modeCreate | CFile::modeReadWrite) == true)
	{
		file.Write(m_EnDeFileData, m_EnDeFileSize);
		file.Close();
		return true;
	}
	else
	{
		::AfxMessageBox(_T("文件创建失败"));
	}
	return  FALSE;
}


//执行文件解密.mxxx格式文件
bool CMXXX_File::File_Decode_exe(CString *Userstring, TBYTE *Ivdata, TBYTE * Keydata, WORD KeySize, bool DesFlat)
{
	if (m_FileData == NULL)
	{
		::AfxMessageBox(_T("未载入密文数据"));
		return  FALSE;
	}
	if (m_EnDeFileData != NULL)
	{
		delete m_EnDeFileData;
		m_EnDeFileData = NULL;
	}
	m_EnDeFileSize = m_FileSize;  //密文解密后长度不变
	m_EnDeFileData = new TBYTE[m_EnDeFileSize];  //申请内存
	memcpy(m_EnDeFileData, m_FileData, m_EnDeFileSize);  //把密文拷贝到需要操作的缓存中
	delete m_FileData;  //释放内存
	m_FileData = NULL;
	if (DesFlat)  //如果是DES解密
	{
		if (Des_Decode_CBCString(Ivdata, m_EnDeFileData, m_EnDeFileData, Keydata, KeySize, m_EnDeFileSize) == false)
		{
			return false;
		}
	}
	else  //AES 解密
	{
		if (Aes_Decode_CBCString(Ivdata, m_EnDeFileData, m_EnDeFileData, Keydata, KeySize, m_EnDeFileSize) == false)
		{
			return false;
		}
	}
	//开始判断是否为 .mxxx格式数据
	TBYTE buff[5];
	memset(buff, 0, 5);
	memcpy(buff, m_EnDeFileData, 4);  //拷贝前4字节数据
	DWORD32 UserCount = *(DWORD32 *)buff;  //转换
	memcpy(buff, &m_EnDeFileData[UserCount + 4], 4);  //拷贝文件大小4字节数据
	DWORD32 Count = *(DWORD32 *)buff;  //转换    明文数据长度
	if ((4 + UserCount + 4 + ((Count % 16) ? ((Count / 16 + 1) * 16) : Count)) == m_EnDeFileSize) //校验是否为mxxx格式文件
	{
		if (UserCount > 0)
		{
			TBYTE *strbuf = new TBYTE[UserCount + 1];
			memset(strbuf, 0, UserCount + 1);
			memcpy(strbuf, &m_EnDeFileData[4], UserCount);
			(*Userstring).Format(_T("%s"), strbuf);
			delete strbuf;
		}
		else
		{
			(*Userstring).Format(_T(""));
		}
		m_EnDeFileSize = Count;  //得到明文长度
		memcpy(m_EnDeFileData, &m_EnDeFileData[8 + UserCount], m_EnDeFileSize);  //拷贝数据明文
	}
	return true;
}



