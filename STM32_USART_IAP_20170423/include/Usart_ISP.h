#ifndef  _USART_ISP_H_
#define  _USART_ISP_H_
#include "afxtempl.h"
#include "My_seriaPort.h"



/*
* STM32 串口 ISP
*/
class Cusart_isp : public CSerialPortCom
{
public:
	Cusart_isp(void);
	~Cusart_isp(void);

public:

	//MCU应答
#define MCU_ACK    0X79
	//MCU非应答
#define MCU_NACK   0X1F
//串口ISP 没个数据包长度   必须为4字节整数倍
#define IspDataSecSize    64
//默认BIN文件的起始地址
#define DefFlashAddr      0X08000000
	//MCU BOOTLOADER版本
	TBYTE m_Bootloader_Ver;
	//MCU 的PID
	DWORD m_Mcu_PID;
	//写FLASH的起始地址
	DWORD m_Upflashaddr;
	/*
	 * 执行升级文件  升级流程
	 */
	bool Updata_file_exe(void);
	//进度条
	CProgressCtrl *m_Prog;
	//进度条指示的静态文本
	CStatic *m_Progtxt;
	//导入升级文件
	bool Install_Upfile_path(LPCTSTR path);
	//当输入缓存为空时 返回Bin文件数据大小
	//当输入缓存不为空时 则填充数据缓存   返回-1 为错误
	int GetFileDataCount(TBYTE *Data = NULL);
	/*
	*发送数据并接收数据
	*send 需要发送的数据 sendlen 发送的数据长度
	*rec 接收数据缓存 reclen接收的数据长度 如果为负数 表示接收长度任意 绝对值为接收缓存大小
	*Maxtime 超时时间 单位 MS
	*/
	bool Send_data_Rec(TBYTE *send, int sendlen, TBYTE *rec, int reclen, DWORD Maxtime = (2*1000));
	//配置串口引脚的RTS及 DTR 引脚  使MCU进入ISP状态
	bool Set_Mcu_IO_to_Isp(void);
	//命令----------------------------------------------------------------------------
	//单片机进入ISP模式
	bool Set_Mcu_to_Isp(void);
	//返回命令 及版本
	bool Get_command(void);
	//返回版本
	bool Get_Version_command(void);
	//返回PID信息
	bool Get_ID_command(void);
	//读指定地址数据 长度最大为256
	bool Read_Memory_command(DWORD addr, TBYTE * BUFF, WORD len);
	//写指定地址数据  长度最大为256   长度必须为4的倍数
	bool Write_Memory_command(DWORD addr, TBYTE * BUFF, WORD len);
	//跳转命令
	bool Go_command(DWORD addr);
	//擦除命令Secbuf 需要擦除的扇区号缓存  number 擦除的扇区数 当为0XFF时 则擦除全部扇区
	bool Erase_Memory_command(TBYTE * Secbuf=NULL, TBYTE number = 0XFF);

	//延伸擦除命令 擦除所有扇区数据  擦除时间较长
	bool Extended_Erase_Memory_command(void);

	//写保护  Secbuf 扇区号缓存  number 扇区数
	bool Write_Protect_command(TBYTE * Secbuf, TBYTE number);
	//去除写保护
	bool Write_Unprotect_command(void);
	//读保护
	bool Readout_Protect_command(void);
	//去除读保护
	bool Readout_Unprotect_command(void);

private:
	//导入HEX升级文件
	bool Install_Hex_Upfile_path(LPCTSTR path);
	//导入bin升级文件
	bool Install_Bin_Upfile_path(LPCTSTR path);
	//检测一个字符串 是否为HEX文件格式
	//Hexbuf 解析后的十六进制数据  len 缓存Hexbuf的长度
	//返回0正常 1缓存长度不足   2不为HEX数据格式
	int Test_HexOneData(CString string,TBYTE *Hexbuf,int len);
	//返回字符数组累加值
	DWORD Return_Add(TBYTE *Data, int len);
	//返回异或检验
	TBYTE Return_Xor(TBYTE *Data, int len);
	//设置进度条
	void Set_Prog_Val(DWORD Val, DWORD max);
private:
	//升级文件路径
	CString Upfpath;
	//升级文件数据数组
	CArray<TBYTE, TBYTE&> Upfdata;
	//升级文件大小
	ULONGLONG   Upfsize;
	//当前文件偏移
	ULONGLONG Upfsizeoff;
	//当前写地址
	DWORD Upwaddr;
};
























#endif