#pragma once
#include "HelpDlg.h"
#include "MXXX_File.h"

// FileDesAes_Dlg 对话框

class FileDesAes_Dlg : public CDialogEx
{
	DECLARE_DYNAMIC(FileDesAes_Dlg)

public:
	FileDesAes_Dlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~FileDesAes_Dlg();
	CHelpDlg * m_help_dlg;     //使用说明 对话框
	CMXXX_File  m_MxxxFile;
// 对话框数据
	enum { IDD = IDD_FileDesAes };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnClose();
	CString m_Userstr;
	CString m_IVStr;
	CString m_KeyStr;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButton5();
	virtual void PostNcDestroy();
	afx_msg void OnBnClickedButton2();
	CString m_Openfilepath;
	afx_msg void OnBnClickedRadiodes();
	afx_msg void OnBnClickedRadioaes();
	afx_msg void OnBnClickedButton3();
	CString m_EnFilePath;
	afx_msg void OnEnChangeEdit9();
	afx_msg void OnBnClickedButton4();
	CString m_Defilepath;
};
