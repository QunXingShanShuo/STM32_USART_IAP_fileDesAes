
// STM32_USART_IAP_20170423.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号


// CSTM32_USART_IAP_20170423App: 
// 有关此类的实现，请参阅 STM32_USART_IAP_20170423.cpp
//

class CSTM32_USART_IAP_20170423App : public CWinApp
{
public:
	CSTM32_USART_IAP_20170423App();

// 重写
public:
	virtual BOOL InitInstance();

// 实现

	DECLARE_MESSAGE_MAP()
};

extern CSTM32_USART_IAP_20170423App theApp;